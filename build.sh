#!/bin/bash

# Change to the script's directory
cd "$(dirname "$(realpath "$0")")";

# Run the build script and make sure it succeeded
if js r.js -o build.js
then
    # Delete the build folder if it exists
    rm -r build

    # Make the build folder
    mkdir build

    # Move the built js file into the build folder
    mv game.js build/game.js

    # Copy the web page into the build folder
    cp index-built.html build/index.html

    # Copy the credits text file into the build folder
    cp credits.txt build/credits.txt

    # Copy all the assets into the build folder
    cp -r ./assets ./build/assets
else
    # Otherwise, don't bother
    echo "Error: build failed."
fi
