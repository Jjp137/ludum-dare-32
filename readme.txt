This is the source code for the Ludum Dare 32 entry by Jjp137 and LegoBricker.
The entry's page can be found here: http://ludumdare.com/compo/ludum-dare-32/?uid=50533

The source code is under the MIT License. See license.txt for more information.

Description:

Some time ago, a scientist pledged to protect his planet. The aliens took a
strange method of attack, and opted to freeze over the sun with super-cooled
 asteroids. The scientist, holding up his pledge, created an orb that could
 eat and transform the asteroids. This game features their battle.

This game was created for Ludum Dare 32, which was based around the theme of
"An Unconventional Weapon", and was created by myself and LegoBricker. In it
you control an orb with 5 abilities, each costing a different amount of energy.
You must use these abilities to try and protect your sun from freezing over
from too much exposure to super-cooled asteroids.
