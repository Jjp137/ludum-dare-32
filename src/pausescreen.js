// Defines the pause screen

define(['phaser', 'utils'], function(Phaser, Utils) {
    'use strict';

    // state must be a GameplayState or else everything breaks
    function PauseScreen(state) {
        this.state = state;
    }

    PauseScreen.COLUMN_Y_START = 230;

    PauseScreen.INSTRUCTIONS_TEXT =
        ["- WASD or Arrow Keys = Move",
         "- Space or Left Mouse Button = Use Skill",
         "- 1-5 = Switch Skill",
         "- M = Mute/Unmute Sound",
         "",
         "- Use the eat ability (1 key) to eat asteroids and gain energy.",
         "- Hold down Space or the Left Mouse Button to eat continuously.",
         "- Eating asteroids makes you full. Fullness can make you move slower.",
         "- Some asteroids give energy more or less efficiently.",
         "- Other skills turn asteroids into weapons, but skills cost energy.",
         "- Skills also take time to recharge. Figure out what each skill does!",
         "- Don't let asteroids reach the sun, or they will make it colder.",
         "- Keep the sun alive as long as you can!"];

    PauseScreen.prototype = {
        constructor: PauseScreen,

        state: null,

        pauseGroup: null,
        background: null,

        paused: null,
        pushToResume: null,
        helpHeader: null,

        instructions: null,

        // Preload all the assets used by the pause screen
        preload: function() {
            this.state.load.image('pause_screen_bg', 'assets/pause_screen_bg.png');
        },

        // Create all the objects that the pause screen is composed of and add
        // them to the state
        create: function() {
            var coords;

            this.pauseGroup = this.state.add.group();
            this.pauseGroup.fixedToCamera = true;

            this.background = this.state.add.image(0, 0, 'pause_screen_bg');
            this.pauseGroup.add(this.background);

            this.paused = this.state.add.text(0, 50, 'Paused',
                {fontSize: "64px", fill: '#FFFFFF'});
            coords = Utils.centerTextAlignPos(this.state.game, this.paused);
            this.paused.x = coords.x;

            this.pushToResume = this.state.add.text(0, 120, 'Push P to Resume',
                {fontSize: "16px", fill: '#FFFFFF'});
            coords = Utils.centerTextAlignPos(this.state.game, this.pushToResume);
            this.pushToResume.x = coords.x;

            this.pauseGroup.add(this.paused);
            this.pauseGroup.add(this.pushToResume);

            this.helpHeader = this.state.add.text(15, 200, 'Instructions',
                {fontSize: "20px", fill: '#FFFFFF'});

            this.pauseGroup.add(this.helpHeader);

            this.instructions = [];
            for (var i = 0; i < PauseScreen.INSTRUCTIONS_TEXT.length; i++) {
                var text = PauseScreen.INSTRUCTIONS_TEXT[i];
                var yPos = PauseScreen.COLUMN_Y_START + i * 20;

                var textObj = this.state.add.text(15, yPos, text,
                    {fontSize: "16px", fill: '#CCCCCC'});
                this.pauseGroup.add(textObj);
                this.instructions.push(textObj);
            }

            this.rightColumn = [];

            // Don't display it initially
            this.pauseGroup.renderable = false;
            this.pauseGroup.visible = false;
        },

        setVisible: function(visible) {
            this.pauseGroup.renderable = visible;
            this.pauseGroup.visible = visible;
        },

        // Call this each frame to update it
        update: function() {

        },

        destroy: function() {
            if (this.pauseGroup) {
                this.pauseGroup.destroy();
                this.pauseGroup = null;
            }

            this.pushToResume = null;
            this.helpHeader = null;

            if (this.instructions) {
                this.instructions = null;
            }
        }
    };

    return PauseScreen;
});
