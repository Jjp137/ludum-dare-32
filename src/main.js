// Starting point of our game

// Taken from: https://github.com/photonstorm/phaser/blob/master/resources/Project%20Templates/RequireJS/src/main.js
(function () {
    'use strict';

    requirejs.config({
        baseUrl: "src/",

        paths: {
            phaser: 'lib/phaser'
        },

        shim: {
            'phaser': {
                exports: 'Phaser'
            }
        }
    });

    require(['phaser', 'game'], function(Phaser, Game) {
        var game = new Game();
        game.start();
    });
}());
