
// States are defined here

define(['phaser', 'pausescreen', 'statusbar', 'things', 'utils'],
function(Phaser, PauseScreen, StatusBar, Things, Utils) {
    'use strict';

    function _TitleState() {

    }

    _TitleState.prototype = Object.create(Phaser.State.prototype);

    _TitleState.prototype.background = null;
    _TitleState.prototype.gameNameText = null;
    _TitleState.prototype.clickText = null;

    _TitleState.prototype.preload = function() {
        this.load.image('title', 'assets/title.jpg');
    };

    _TitleState.prototype.create = function() {
        var coords;

        this.background = this.add.image(0, 0, 'title');

        this.gameNameText = this.add.text(100, 100, 'SOLAR GUARDIAN',
            {fontSize: "85px", fill: '#FFFFFF'});
        coords = Utils.centerTextAlignPos(this.game, this.gameNameText);
        this.gameNameText.x = coords.x;

        this.clickText = this.add.text(100, 250, 'Push Space to Start',
            {fontSize: "32px", fill: '#CCCC00'});
        coords = Utils.centerTextAlignPos(this.game, this.clickText);
        this.clickText.x = coords.x;
    };

    _TitleState.prototype.update = function() {
        if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.game.state.start('story');
        }
    };

    _TitleState.prototype.shutdown = function() {
        if (this.background) {
            this.background.destroy();
            this.background = null;
        }

        if (this.gameNameText) {
            this.gameNameText.destroy();
            this.gameNameText = null;
        }

        if (this.clickText) {
            this.clickText.destroy();
            this.clickText = null;
        }
    };

    function _StoryState() {

    }

    _StoryState.prototype = Object.create(Phaser.State.prototype);

    // Text for the skipping and proceeding instructions
    _StoryState.prototype.skipText = null;
    _StoryState.prototype.proceedText = null;

    // Text to hold the story and respective counter
    _StoryState.prototype.storyText = null;
    _StoryState.prototype.storyCounter = null;

    // Keys for skipping and proceeding
    _StoryState.prototype.skipKey = null;
    _StoryState.prototype.proceedKey = null;

    _StoryState.prototype.preload = function() {

    };

    _StoryState.prototype.create = function() {
        // Initialize the text
        this.skipText = this.add.text(0, 475, 'Push Enter to Skip',
            {fontSize: "20px", fill: '#FFFFFF'});
        this.proceedText = this.add.text(450, 465, 'Push Space to Continue',
            {fontSize: "30px", fill: '#FFFFFF'});
        this.storyText = this.add.text(100, 90, '',
            {fontSize: "50px", fill: '#FFFFFF', wordWrap: true,
             wordWrapWidth: 650, align: 'center'});

        // Set the story counter to 0 to start.
        this.storyCounter = 0;

        // Set up the keys to use.
        this.skipKey = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.skipKey.onDown.add(this.goToGame, this);

        this.changeText();
        this.proceedKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.proceedKey.onDown.add(this.changeText, this);
    };

    _StoryState.prototype.update = function() {

    };

    _StoryState.prototype.goToGame = function() {
        this.game.state.start('gameplay');
    };

    _StoryState.prototype.changeText = function() {
        var texts = ['Many decades ago there was a scientist who worked on a planet near a sun.',
                     'One day some aliens decided to use a most unusual method to attack his planet.',
                     'They planned to freeze the sun with super-cooled asteroids.',
                     'But the scientist responded with an equally unusual method.',
                     'An orb that could either eat or transform the asteroids.',
                     'And so a great battle was waged.'];

        if (this.storyCounter > texts.length - 1) {
            this.game.state.start('gameplay');
            return;
        }

        this.storyText.text = texts[this.storyCounter];
        this.storyCounter++;
    };

    _StoryState.prototype.shutdown = function() {
        if (this.skipText) {
            this.skipText.destroy();
            this.skipText = null;
        }

        if (this.proceedText) {
            this.proceedText.destroy();
            this.proceedText = null;
        }

        if (this.storyText) {
            this.storyText.destroy();
            this.storyText = null;
        }
        this.storyCounter = null;

        if (this.skipKey) {
            this.skipKey.reset();
            this.skipKey = null;
        }

        if (this.proceedKey) {
            this.proceedKey.reset();
            this.proceedKey = null;
        }
    };

    function _GameplayState() {

    };

    // Constants
    _GameplayState.RIGHT_BOUNDS = 2000;

    _GameplayState.prototype = Object.create(Phaser.State.prototype);

    _GameplayState.prototype.player = null;
    _GameplayState.prototype.sun = null;
    _GameplayState.prototype.asteroids = null;
    _GameplayState.prototype.cores = null;
    _GameplayState.prototype.projectiles = null;  // Any projectiles the player makes
    _GameplayState.prototype.rightWall = null;

    //_GameplayState.prototype.fpsText = null;
    _GameplayState.prototype.statusBar = null;
    _GameplayState.prototype.levelText = null;
    _GameplayState.prototype.levelTextTimer = null;

    _GameplayState.prototype.skillBox = null;

    // The asteroids in the current frame that the player can reach and act upon;
    // remember to clear this every frame!
    _GameplayState.prototype.targetAsteroids = null;

    // Controls difficulty progression
    _GameplayState.prototype.timeElapsed = null;  // in ms
    _GameplayState.prototype.level = null;
    _GameplayState.prototype.curSpawnDelayBase = null; // in ms
    _GameplayState.prototype.curAsteroidLimit = null;
    _GameplayState.prototype.curSpawnDelay = null;

    // Music and sounds
    _GameplayState.prototype.bgMusic = null;
    _GameplayState.prototype.coreGetSound = null;
    _GameplayState.prototype.freezingSound = null;
    _GameplayState.prototype.missileHitSound = null;
    _GameplayState.prototype.missileShootSound = null;
    _GameplayState.prototype.playerEatSound = null;
    _GameplayState.prototype.noUseSound = null;

    _GameplayState.prototype.turretCreateSound = null;
    _GameplayState.prototype.jamCreateSound = null;
    _GameplayState.prototype.jamAttackSound = null;

    _GameplayState.prototype.muteKey = null;

    // Keeps track of if at least one asteroid has overlapped the sun this
    // frame; used for stopping the sound early
    _GameplayState.prototype.isFreezing = null;

    // Pause screen stuff
    _GameplayState.prototype.pauseScreen = null;
    _GameplayState.prototype.gamePaused = null;
    _GameplayState.prototype.pauseKey = null;

    // Called first before anything else
    _GameplayState.prototype.init = function() {

    };

    _GameplayState.prototype.preload = function() {
        this.load.image('player', 'assets/player.png');
        this.load.image('sun', 'assets/sun.png');
        this.load.image('asteroid', 'assets/asteroid.png');
        this.load.image('better_ast', 'assets/better_asteroid.png');
        this.load.image('worse_ast', 'assets/worse_asteroid.png');
        this.load.image('asteroid_core', 'assets/asteroid_core.png');
        this.load.image('missile', 'assets/missile.png');
        this.load.image('homing_missile', 'assets/homing_missile.png');
        this.load.image('skill_range', 'assets/skill_range.png');
        this.load.image('invisible', 'assets/invisible.png');
        this.load.image('game_background', 'assets/game_bg.png');
        this.load.image('turret', 'assets/turret.png');
        this.load.image('jam', 'assets/jam.png');

        this.load.audio('ambience', 'assets/music.ogg');

        this.load.audio('core_get', 'assets/core_get.ogg');
        this.load.audio('freezing', 'assets/freezing.ogg');
        this.load.audio('missile_hit', 'assets/missile_hit.ogg');
        this.load.audio('missile_shoot', 'assets/missile_shoot.ogg');
        this.load.audio('player_eat', 'assets/player_eat.ogg');
        this.load.audio('no_use', 'assets/no_use.ogg');
        this.load.audio('turret_create', 'assets/turret_create.ogg');
        this.load.audio('jam_create', 'assets/jam_create.ogg');
        this.load.audio('jam_attack', 'assets/jam_attack.ogg');

        this.statusBar = new StatusBar(this);
        this.statusBar.preload();

        this.pauseScreen = new PauseScreen(this);
        this.pauseScreen.preload();
    };

    _GameplayState.prototype.create = function() {
        // Turns on fps calculations; comment this out before release
        this.time.advancedTiming = true;

        // Create the asteroids array
        this.targetAsteroids = [];

        // Initialize the level progression
        this.timeElapsed = 0;  // in ms
        this.level = 1;
        this.curSpawnDelayBase = 2000; // in ms
        this.curAsteroidLimit = 10;
        this.curSpawnDelay = 0;

        // Give the background a repeating star background.
        this.game.add.tileSprite(0, 0, 2400, 800, 'game_background');

        // Load in the music and start playing it.
        this.bgMusic = this.add.audio('ambience', 1, true);
        this.bgMusic.play();

        // Load every sound ever
        this.coreGetSound = this.add.audio('core_get');
        this.freezingSound = this.add.audio('freezing');
        this.missileHitSound = this.add.audio('missile_hit');
        this.missileShootSound = this.add.audio('missile_shoot');
        this.playerEatSound = this.add.audio('player_eat');
        this.noUseSound = this.add.audio('no_use');
        this.turretCreateSound = this.add.audio('turret_create');
        this.jamCreateSound = this.add.audio('jam_create');
        this.jamAttackSound = this.add.audio('jam_attack');

        // Bind the mute button
        this.muteKey = this.input.keyboard.addKey(Phaser.Keyboard.M);
        this.muteKey.onDown.add(function() {
            this.sound.mute = !this.sound.mute;
        }, this);

        // Set the world bounds to be longer and a bit higher
        this.world.bounds = new Phaser.Rectangle(0, 0, 2400, 800);
        // Adjust the camera
        this.camera.setBoundsToWorld();
        this.camera.setPosition(0, 0);
        // 2000-2400 on the x coordinate is reserved for asteroid spawning
        this.camera.bounds.width -= 400;

        // Enable physics
        this.physics.startSystem(Phaser.Physics.ARCADE);

        // Create an invisible wall that blocks the player from camping
        // the asteroid spawning point
        this.rightWall = this.add.sprite(_GameplayState.RIGHT_BOUNDS, 0, 'invisible');
        this.rightWall.scale.setTo(10, this.world.height);  // Make it tall enough
        this.physics.arcade.enable(this.rightWall);

        this.rightWall.renderable = false;  // Don't render the graphic
        this.rightWall.body.immovable = true; // Otherwise, you can push the wall

        // Create the sun first
        this.sun = new Things.Sun(this.game, 0, 100);
        this.add.existing(this.sun);

        // Then create the skill range box
        this.skillBox = this.add.image(0, 0, 'skill_range');

        // Then create the projectiles group
        this.projectiles = this.add.group();
        this.projectiles.enableBody = true;

        // Then create the player
        this.player = new Things.Player(this.game, 800, 200);
        this.add.existing(this.player);

        // Create the asteroids group
        this.asteroids = this.add.group();
        // Tell create() and similar methods to pump out Asteroid objects
        // instead of Sprite objects
        this.asteroids.classType = Things.Asteroid;
        this.asteroids.enableBody = true;

        // Create the cores group
        this.cores = this.add.group();
        this.cores.enableBody = true;

        // Add an FPS counter
        /*this.fpsText = this.add.text(0, 0, "",
            {fontSize: "16px", fill: "#00ff00"});
        // Fix that to the camera
        this.fpsText.fixedToCamera = true;*/

        // Level number text
        this.levelText = this.add.text(0, 80, "",
            {fontSize: "64px", fill: "#FFFFFF"});
        this.levelText.text = "Level 1";
        var textCoords = Utils.centerTextAlignPos(this.game, this.levelText);
        this.levelText.x = textCoords.x;
        this.levelText.y = this.game.height - 200;
        this.levelText.fixedToCamera = true;

        // Create the statusbar
        this.statusBar.create();

        // Make the camera follow the player
        this.camera.follow(this.player);
        // Define a rectangle where the camera doesn't move if the player is
        // moving within it; and yes I know about all those magic numbers :(
        this.camera.deadzone = new Phaser.Rectangle(360, 225, 80, 50);

        // Make the pause screen last so that it appears on top of everything
        this.pauseScreen.create();
        this.gamePaused = false;
        this.pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.P);
        this.pauseKey.onDown.add(this.togglePause, this);
    };

    _GameplayState.prototype.update = function() {
        // Clear the list of eligible asteroids first
        this.targetAsteroids.length = 0; // Yes, this works. js is the future -.-

        //this.fpsText.text = "FPS: " + this.time.fps;

        // Don't process anything if the game is paused
        if (this.gamePaused) {
            return;
        }

        this.readInput();
        this.advanceTime();
        this.runCollision();

        // Run any 'AI' that projectiles may have
        this.projectiles.forEach(function(child){
            child.think(this);
        }, this);

        this.attemptAsteroidSpawn();

        this.statusBar.update();
        this.cleanup();

        this.checkForGameOver();
    };

    _GameplayState.prototype.togglePause = function() {
        this.gamePaused = !this.gamePaused;

        if (this.gamePaused) {
            this.physics.arcade.isPaused = true;
            this.pauseScreen.setVisible(true);
            this.sound.pauseAll();
        }
        else {
            this.physics.arcade.isPaused = false;
            this.pauseScreen.setVisible(false);
            this.sound.resumeAll();
        }
    }

    // Decrement the various timers and cooldowns
    // This is done in game-time, not real-time
    _GameplayState.prototype.advanceTime = function() {
        var delta = this.time.physicsElapsedMS;  // milliseconds

        // Decrease cooldowns
        this.player.subtractCooldowns(delta);
        this.sun.subtractCooldowns(delta);

        // Remove all cores that have fully decayed
        var cores = this.cores.children.slice(0); // Make a copy of the array
        for (var i = 0; i < cores.length; i++) {
            cores[i].decay(delta);

            if (cores[i].curValue <= 0) {
                this.cores.remove(cores[i], true);
            }
        }

        // Decrease the delay until the next asteroid spawns
        this.curSpawnDelay -= delta;

        // Increase the time that has elapsed
        this.timeElapsed += delta;

        // Clears the text after it has been on screen for 4 seconds
        if ((this.timeElapsed - this.levelTextTimer) > 4 * 1000) {
            this.levelText.text = "";
        }

        if (this.timeElapsed > this.level * 30 * 1000) {
            this.curSpawnDelayBase = Math.floor(0.9 * this.curSpawnDelayBase);
            this.curAsteroidLimit = Math.floor(1.2 * this.curAsteroidLimit);
            this.level += 1;

            // Let the player know that the level increased
            this.levelText.text = "Level " + this.level;
            this.levelTextTimer = this.timeElapsed;
        }
    };

    _GameplayState.prototype.runCollision = function() {
        var deadAsteroids;
        var deadProjectiles;
        var deadCores;

        // Make sure that the player can bump into the right wall
        this.physics.arcade.collide(this.player, this.rightWall);

        // Make sure that overlap is checked between the sun and the asteroids
        this.isFreezing = false;

        this.physics.arcade.overlap(this.sun, this.asteroids,
            // Third parameter: Function to call if overlap occurs
            // If it is a Sprite vs. Group, the sprite is always passed first
            function(sun, ast) {
                sun.damage(1);
                ast.damage(1);

                // Note that some health was lost because the sun was defending itself
                ast.sunDamageTaken += 1;

                // Play the sound here.
                this.isFreezing = true;
                Utils.playSoundIfIdle(this.freezingSound);
            },
            // Fourth parameter: Another function to call if overlap occurs
            // This is meant for additional processing and checking. If this is
            // present, then the function defined in the third parameter won't
            // run if this returns false. In this case, we don't need to do any
            // more processing so we always return true here.
            function(sun, ast) {
                return true;
            },
            // Fifth parameter: the "context" for the previous two functions
            // In other words, this parameter sets what the keyword "this"
            // refers to in the previous two functions. We need to set this
            // parameter to the GameplayState object itself because otherwise
            // the functions won't be able to see its variables.
            this);

        // Stop playing the sound if nothing overlapped this frame.
        if (!this.isFreezing) {
            this.freezingSound.stop();
        }

        // Remove all dead asteroids in this step; removing while checking
        // for overlap might result in errors since the whole list is being
        // iterated through.
        deadAsteroids = this.asteroids.filter(
            function(child, index, children) {
                return child.health <= 0;
            });

        for (var i = 0; i < deadAsteroids.total; i++) {
            this.asteroids.remove(deadAsteroids.list[i], true);
        }

        // Make sure that overlap is checked between projectiles and asteroids
        this.physics.arcade.overlap(this.projectiles, this.asteroids,
            function(proj, ast) {
                // Don't let one projectile hit more targets than it was
                // defined to hit
                if (!proj.checkIfAlreadyHit()) {
                    proj.hitAsteroid(ast);
                }
            },
            function(proj, ast) {
                return true;
            }, this);

        // And again...
        deadAsteroids = this.asteroids.filter(
            function(child, index, children) {
                return child.health <= 0;
            });

        for (var i = 0; i < deadAsteroids.total; i++) {
            // Spawn cores from asteroids killed by projectiles except from
            // those that have been exposed to the sun
            var core = Things.Core.fromAsteroid(deadAsteroids.list[i]);
            this.cores.add(core);

            this.asteroids.remove(deadAsteroids.list[i], true);
        }

        // Same with the projectiles
        deadProjectiles = this.projectiles.filter(
            function(child, index, children) {
                return child.health <= 0;
            });

        for (var i = 0; i < deadProjectiles.total; i++) {
            this.projectiles.remove(deadProjectiles.list[i], true);
        }

        // Make sure that the player picks up cores
        this.physics.arcade.overlap(this.player, this.cores,
            function(player, core) {
                core.obtained = true;
                player.energy += core.curValue;

                // Play the sound
                Utils.playSoundIfIdle(this.coreGetSound);
            },
            function(player, core) {
                return true;
            },
            this);

        // Delete any cores that were picked up
        deadCores = this.cores.filter(
            function(child, index, children) {
                return child.obtained;
            });

        for (var i = 0; i < deadCores.total; i++) {
            this.cores.remove(deadCores.list[i], true);
        }
    };

    _GameplayState.prototype.readInput = function() {
        var keyboard = this.input.keyboard;  // I'm not typing that every time
        var vector = new Phaser.Point(0, 0);

        // Arrow keys are included b/c I remember some people in PyGame complaining
        // that WASD does not match w/ their keyboard layout at all, so arrow keys
        // will at least make the game playable for those people
        if (keyboard.isDown(Phaser.Keyboard.A) || keyboard.isDown(Phaser.Keyboard.LEFT)) {
            vector.x -= 1;
        }
        if (keyboard.isDown(Phaser.Keyboard.D) || keyboard.isDown(Phaser.Keyboard.RIGHT)) {
            vector.x += 1;
        }
        if (keyboard.isDown(Phaser.Keyboard.W) || keyboard.isDown(Phaser.Keyboard.UP)) {
            vector.y -= 1;
        }
        if (keyboard.isDown(Phaser.Keyboard.S) || keyboard.isDown(Phaser.Keyboard.DOWN)) {
            vector.y += 1;
        }

        // Normalize the vector first and then set its length to the player's speed
        // This shouldn't exhibit the problem of moving faster diagonally, but lego should
        // double check the math b/c idk my math anymore, although the console.log
        // output looks right to me
        vector.normalize();
        vector.setMagnitude(this.player.getSpeed());

        this.player.setVelocity(vector.x, vector.y);

        // If a number key is down, change to that skill
        if (keyboard.isDown(Phaser.Keyboard.ONE)) {
            this.player.changeSkill(0);
        }
        else if (keyboard.isDown(Phaser.Keyboard.TWO)) {
            this.player.changeSkill(1);
        }
        else if(keyboard.isDown(Phaser.Keyboard.THREE)) {
            this.player.changeSkill(2);
        }
        else if(keyboard.isDown(Phaser.Keyboard.FOUR)) {
            this.player.changeSkill(3);
        }
        else if(keyboard.isDown(Phaser.Keyboard.FIVE)) {
            this.player.changeSkill(4);
        }

        // Update the position of the rectangle that denotes the skill's range
        this.player.updateSkillRect();

        if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) ||
            this.input.mouse.button === Phaser.Mouse.LEFT_BUTTON) {
            // Find all asteroids that are within the skill's range and
            // populate the list for the skill to be used
            this.asteroids.forEach(function(ast) {
                // I couldn't find a method to get the rectangle from the body so
                // I have to make a new variable just for that
                var astRect = new Phaser.Rectangle(ast.x, ast.y, ast.width, ast.height);

                // Since an asteroid's anchor is (0.5, 0.5), adjust the x
                // and y values accordingly
                astRect.x -= astRect.halfWidth;
                astRect.y -= astRect.halfHeight;

                if (this.player.curSkillRect.intersects(astRect)) {
                    this.targetAsteroids.push(ast);
                }
            }, this);

            this.player.useSkill(this);
        }
    };

    _GameplayState.prototype.attemptAsteroidSpawn = function() {
        // Don't spawn asteroids if there are too many or it is not time yet
        if (this.curSpawnDelay > 0 ||
            this.asteroids.total >= this.curAsteroidLimit) {
            return;
        }

        // 95% chance not to spawn an asteroid this frame
        if (this.rnd.integerInRange(1, 100) > 5) {
            return;
        }

        // Reset the spawn delay
        this.curSpawnDelay = Math.max(250, Math.floor(
                    this.rnd.realInRange(0.6, 1.1) * this.curSpawnDelayBase));

        // Make sure that the asteroid is within the spawning area
        var x = this.world.width - this.rnd.integerInRange(100, 300);
        var y = this.rnd.integerInRange(20, this.world.height - 20);

        var type = Things.Asteroid.NORMAL;
        var key = 'asteroid';
        var rand = this.rnd.integerInRange(1, 10);

        // 20% chance that the asteroid yields more energy
        if (rand <= 2) {
            type = Things.Asteroid.MORE_ENERGY;
            key = 'better_ast';
        }
        // If the above fails, 30% chance that the asteroid yields less energy
        // if we are on level 4 or higher
        else if (this.level >= 4 && rand >= 3 && rand <= 5) {
            type = Things.Asteroid.LESS_ENERGY;
            key = 'worse_ast';
        }

        // Create the appropriate asteroid and randomize its stats
        var ast = this.asteroids.create(x, y, key);
        ast.type = type;
        ast.randomizeStats();
    };

    // Mainly deletes things that are no longer within the world and do
    // other housekeeping
    _GameplayState.prototype.cleanup = function() {
        // Obtain and remove all asteroids that are out of bounds
        var outOfBoundsAsteroids = this.asteroids.filter(
            function(child, index, children) {
                return !Utils.inWorld(child.game.world, child);
            }
        );

        for (var i = 0; i < outOfBoundsAsteroids.total; i++) {
            this.asteroids.remove(outOfBoundsAsteroids.list[i], true);
        }

        // Obtain and remove all projectiles that are out of bounds
        var outOfBoundsProjs = this.projectiles.filter(
            function(child, index, children) {
                return !Utils.inWorld(child.game.world, child);
            }
        );

        for (var i = 0; i < outOfBoundsProjs.total; i++) {
            this.projectiles.remove(outOfBoundsProjs.list[i], true);
        }

        // Set the position of the rendered box that indicates the current
        // skill's range
        this.skillBox.x = this.player.curSkillRect.x;
        this.skillBox.y = this.player.curSkillRect.y;
        this.skillBox.width = this.player.curSkillRect.width;
        this.skillBox.height = this.player.curSkillRect.height;

        // Don't draw the rectangle if the skill doesn't support it or
        // if the cooldown is still active
        var drawRect = this.player.curSkill.drawRange;

        // Indicate to the player if the cooldown is active
        this.skillBox.tint = this.player.curSkill.cooldown > 0 ?
                             0xFF0000 : 0xFFFFFF;

        // I really don't know the difference, so just set both
        this.skillBox.renderable = drawRect;
        this.skillBox.visible = drawRect;
    };

    _GameplayState.prototype.checkForGameOver = function() {
        if (this.sun.health <= 0) {
            // Stop the music
            this.bgMusic.stop();

            // Fourth parameters and onwards are passed to the init() function
            this.game.state.start('gameover', true, false, this.timeElapsed,
                                  this.sound.mute);
            return;
        }
    };

    // Called when a new state is about to begin; do cleanup here, and yes
    // this method is stupidly long
    _GameplayState.prototype.shutdown = function() {
        if (this.targetAsteroids) {
            this.targetAsteroids = null;
        }

        if (this.player) {
            this.player.destroy();
            this.player = null;
        }

        if (this.sun) {
            this.sun.destroy();
            this.sun = null;
        }

        if (this.asteroids) {
            this.asteroids.destroy();
            this.asteroids = null;
        }

        if (this.cores) {
            this.cores.destroy();
            this.cores = null;
        }

        if (this.projectiles) {
            this.projectiles.destroy();
            this.projectiles = null;
        }

        if (this.rightWall) {
            this.rightWall.destroy();
            this.rightWall = null;
        }

        /*if (this.fpsText) {
            this.fpsText.destroy();
            this.fpsText = null;
        }*/

        if (this.statusbar) {
            this.statusbar.destroy();
            this.statusbar = null;
        }

        if (this.levelText) {
            this.levelText.destroy();
            this.levelText = null;
        }
        this.levelTextTimer = null;

        if (this.skillBox) {
            this.skillBox.destroy();
            this.skillBox = null;
        }

        this.timeElapsed = null;
        this.level = null;
        this.curSpawnDelayBase = null;
        this.curAsteroidLimit = null;
        this.curSpawnDelay = null;

        this.sound.destroy();
        this.bgMusic = null;
        this.coreGetSound = null;
        this.freezingSound = null;
        this.missileHitSound = null;
        this.missileShootSound = null;
        this.playerEatSound = null;
        this.noUseSound = null;
        this.turretCreateSound = null;
        this.jamCreateSound = null;
        this.jamAttackSound = null;

        this.isFreezing = null;

        if (this.pauseScreen) {
            this.pauseScreen.destroy();
            this.pauseScreen = null;
        }
        this.gamePaused = null;

        if (this.pauseKey) {
            this.pauseKey.reset();
            this.pauseKey = null;
        }

        if (this.muteKey) {
            this.muteKey.reset();
            this.muteKey = null;
        }
    };

    function _GameOverState() {

    }

    _GameOverState.prototype = Object.create(Phaser.State.prototype);

    _GameOverState.prototype.timeSurvived = null;
    _GameOverState.prototype.isMuted = null;

    _GameOverState.prototype.background = null;

    _GameOverState.prototype.resumeKey = null;

    _GameOverState.prototype.gameOverText = null;
    _GameOverState.prototype.timeText = null;
    _GameOverState.prototype.restartText = null;

    _GameOverState.prototype.gameOverSound = null;

    _GameOverState.prototype.init = function(elaspedTime, isMuted) {
        this.timeSurvived = elaspedTime;
        this.isMuted = isMuted;
    };

    _GameOverState.prototype.preload = function() {
        this.load.image('title', 'assets/title.jpg');

        this.load.audio('game_over', 'assets/game_over.ogg');
    };

    _GameOverState.prototype.create = function() {
        var coords;

        this.background = this.add.image(0, 0, 'title');

        this.gameOverText = this.add.text(100, 100, 'GAME OVER',
            {fontSize: "95px", fill: '#FFFFFF'});
        coords = Utils.centerTextAlignPos(this.game, this.gameOverText);
        this.gameOverText.x = coords.x;

        this.timeText = this.add.text(100, 200, "",
            {fontSize: "36px", fill: '#FFFFFF'});
        this.timeText.text = "Time survived: " + Utils.msToString(this.timeSurvived);
        coords = Utils.centerTextAlignPos(this.game, this.timeText);
        this.timeText.x = coords.x;

        this.resumeKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.resumeKey.onDown.add(function() {
            this.game.state.start('title');
            }, this);

        this.restartText = this.add.text(100, 300, "Push Space to Restart",
            {fontSize: "36px", fill: '#CCCC00'});
        coords = Utils.centerTextAlignPos(this.game, this.restartText);
        this.restartText.x = coords.x;

        this.gameOverSound = this.add.audio('game_over');
        if (!this.isMuted) {
            this.gameOverSound.play();
        }
    };

    _GameOverState.prototype.update = function() {

    };

    _GameOverState.prototype.shutdown = function() {
        if (this.background) {
            this.background.destroy();
            this.background = null;
        }

        this.timeSurvived = null;

        if (this.resumeKey) {
            this.resumeKey.reset();
            this.resumeKey = null;
        }

        if (this.gameOverText) {
            this.gameOverText.destroy();
            this.gameOverText = null;
        }

        if (this.timeText) {
            this.timeText.destroy();
            this.timeText = null;
        }

        if (this.restartText) {
            this.restartText.destroy();
            this.restartText = null;
        }

        if (this.gameOverSound) {
            this.gameOverSound.destroy();
            this.gameOverSound = null;
        }
    };

    var states = {TitleState: _TitleState, StoryState: _StoryState,
                  GameplayState: _GameplayState, GameOverState: _GameOverState};

    return states;
});
