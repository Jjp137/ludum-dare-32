// For miscellaneous functions

define(['phaser'], function(Phaser) {
    // Written b/c Sprite.getBounds() is currently buggy
    function _inWorld(world, sprite) {
        var spriteRect = new Phaser.Rectangle(sprite.x, sprite.y,
                                              sprite.width, sprite.height);
        spriteRect.x -= sprite.anchor.x * sprite.width;
        spriteRect.y -= sprite.anchor.y * sprite.height;

        return world.bounds.intersects(spriteRect);
    }

    function _msToString(time) {
        var timeSeconds = Math.floor(time / 1000);
        var minutesDisplay = Math.floor(timeSeconds / 60);
        var secondsDisplay = timeSeconds % 60;

        if (secondsDisplay < 10) {
            // Converts it to a string; js is the future -.-
            secondsDisplay = "0" + secondsDisplay;
        }

        return minutesDisplay + ":" + secondsDisplay;
    }

    // Given a frame where the text resides and the text itself, find the
    // position of the upper left corner that can be used to center the text
    // on the screen
    function _centerTextAlignPos(outer, text) {
        return new Phaser.Point(Math.floor(outer.width / 2 - text.width / 2),
                                Math.floor(outer.height / 2 - text.height / 2));
    }

    function _playSoundIfIdle(sound) {
        if (!sound.isPlaying) {
            sound.play();
        }
    }

    var utils = {inWorld: _inWorld,
                 msToString: _msToString,
                 centerTextAlignPos: _centerTextAlignPos,
                 playSoundIfIdle: _playSoundIfIdle};

    return utils;
});
