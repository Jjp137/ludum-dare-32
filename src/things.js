// Gameplay 'things' are defined here

define(['phaser', 'skills', 'utils'], function(Phaser, Skills, Utils) {
    'use strict';

    // Key and frame are unused are now (may change later)
    function _Player(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'player');

        // Enable physics and set some properties
        game.physics.arcade.enable(this);
        this.body.collideWorldBounds = true;

        // Initialize every skill
        this.allSkills = [new Skills.EatSkill(),
                          new Skills.MissileSkill(),
                          new Skills.HomingMissileSkill(),
                          new Skills.TurretSkill(),
                          new Skills.JamSkill()];

        // Set the initial skill and obtain its area
        this.curSkill = this.allSkills[0];
        this.curSkillIndex = 0;
        this.curSkillRect = this.curSkill.getAreaRect();
    }

    // Constants; note that prototype is not used b/c otherwise it would belong
    // to each object
    _Player.MAX_SPEED = 300;
    _Player.DIAMETER = 64;

    // Max energy value
    _Player.MAX_ENERGY = 2000;
    // Base energy gain rate
    _Player.ENERGY_GAIN_RATE = 0.25;

    // Max fullness value
    _Player.MAX_FULLNESS = 200;
    // Base fullness gain rate
    _Player.FULLNESS_GAIN_RATE = 0.10;

    // Delay value to set upon any consumption of asteroids (in ms)
    _Player.FULLNESS_DELAY_ON_EAT = 3000;
    // When fullness is emptying, delay between 0.5% of the fullness dropping (in ms)
    _Player.FULLNESS_REGEN_INTERVAL = 500;

    _Player.prototype = Object.create(Phaser.Sprite.prototype);

    // Energy of the player
    _Player.prototype.energy = 0;

    // Fullness of the player
    _Player.prototype.fullness = 0;
    // Cooldown before fullness starts dropping (in ms)
    _Player.prototype.fullnessDelay = 0;

    // Current skill
    _Player.prototype.curSkill = null;
    _Player.prototype.curSkillIndex = 0;
    // All skills
    _Player.prototype.allSkills = null;
    // Bounding box of current skill; is not centered to the player
    _Player.prototype.curSkillRect = null;
    // For not playing the "can't use" sound too soon
    _Player.prototype.lastUseTime = 0;

    // State should be a GameplayState
    _Player.prototype.useSkill = function(state) {
        // Don't do anything if the cooldown has not expired or the player
        // does not have enough energy
        if (this.curSkill.cooldown > 0 ||
            Math.floor(this.energy) < this.curSkill.cost ||
            state.targetAsteroids.length === 0) {
            // Prevent the "can't use" sound from playing too soon
            if (this.lastUseTime <= 0) {
                Utils.playSoundIfIdle(state.noUseSound);
            }
            return;
        }

        // Otherwise, use the skill
        this.energy -= this.curSkill.cost;
        this.curSkill.use(state);

        // Note that the skill has been used
        this.lastUseTime = 500;
    };

    _Player.prototype.changeSkill = function(id) {
        // Don't do anything if the skill is the same
        if (id === this.curSkillIndex) {
            return;
        }

        this.curSkill = this.allSkills[id];
        this.curSkillIndex = id;
        this.curSkillRect = this.curSkill.getAreaRect();

        // If the player switches, then allow the "can't use" sound to play
        // again
        this.lastUseTime = 0;
    };

    // Call this every frame to make sure that the rectangle is correct
    _Player.prototype.updateSkillRect = function() {
        // Center the rectangle around the player
        this.curSkillRect.centerX = this.body.center.x;
        this.curSkillRect.centerY = this.body.center.y;
    };

    _Player.prototype.subtractCooldowns = function(ms) {
        // Lower cooldown of all abilities
        for (var i = 0; i < this.allSkills.length; i++) {
            this.allSkills[i].cooldown -= ms;
            if (this.allSkills[i].cooldown < 0) {
                this.allSkills[i].cooldown = 0;
            }
        }

        // Lower the fullness-related cooldown
        this.fullnessDelay -= ms;
        if (this.fullnessDelay <= 0) {
            this.fullness -= 1;

            if (this.fullness < 0) {
                this.fullness = 0;
            }

            // Wait again
            this.fullnessDelay = _Player.FULLNESS_REGEN_INTERVAL;
        }

        // Lower the delay before the "can't use" sound can be heard again
        this.lastUseTime -= ms;
        if (this.lastUseTime < 0) {
            this.lastUseTime = 0;
        }
    };

    _Player.prototype.gainEnergy = function(val) {
        this.energy += val;

        if (Math.floor(this.energy) >= _Player.MAX_ENERGY) {
            this.energy = _Player.MAX_ENERGY;
        }
    };

    _Player.prototype.gainFullness = function(val) {
        this.fullness += val;

        if (Math.floor(this.fullness) >= _Player.MAX_FULLNESS) {
            this.fullness = _Player.MAX_FULLNESS;
        }

        // Reset the delay
        this.fullnessDelay = _Player.FULLNESS_DELAY_ON_EAT;
    };

    _Player.prototype.getSpeed = function() {
        var halfFullness = _Player.MAX_FULLNESS / 2;
        var penalty = (Math.max(0, (this.fullness - halfFullness) * 1.0)) /
                      halfFullness * (0.4 * _Player.MAX_SPEED);

        return _Player.MAX_SPEED - penalty;
    }

    _Player.prototype.setVelocity = function(x, y) {
        this.body.velocity.x = x;
        this.body.velocity.y = y;
    };

    function _Asteroid(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, key, frame);

        // So that the asteroid shrinks towards the center
        this.anchor.setTo(0.5, 0.5);
    }

    // Types of asteroids
    _Asteroid.NORMAL = 0;
    _Asteroid.MORE_ENERGY = 1;
    _Asteroid.LESS_ENERGY = 2;

    _Asteroid.prototype = Object.create(Phaser.Sprite.prototype);

    // Set this immediately after you construct an Asteroid object
    _Asteroid.prototype.type = _Asteroid.NORMAL;
    // Health should be set to the number of frames that an asteroid
    // should last; so 60 = 1 second
    _Asteroid.prototype.health = 120;
    _Asteroid.prototype.maxHealth = 120;
    // So that anything that does precise targeting is smart enough to
    // not go after the same thing
    _Asteroid.prototype.targeted = false;
    // Here's a lesson: if you initialize an object in the prototype,
    // all of the objects share the prototype :( so don't do that
    _Asteroid.prototype.origScaleFactor = 1.0;
    // Health lost by the sun or projectiles; used for energy calculations
    _Asteroid.prototype.sunDamageTaken = 0;

    // Call this if you don't want to set the stats yourself
    _Asteroid.prototype.randomizeStats = function() {
        // Asteroids can be from 30% to 100% size; it's better to scale
        // down than to scale upwards
        this.origScaleFactor = this.game.rnd.realInRange(0.3, 1.0);
        this.scale = new Phaser.Point(this.origScaleFactor, this.origScaleFactor);

        // Health is based on how large the asteroid ends up becoming
        this.health = Math.floor(this.origScaleFactor * 120);
        this.maxHealth = this.health;

        // Give the asteroid a random speed
        this.body.velocity.x = this.game.rnd.integerInRange(-250, -100);
        this.body.velocity.y = this.game.rnd.integerInRange(-30, 30);
    };

    // Decreases the asteroid's health by the given amount
    _Asteroid.prototype.damage = function(value) {
        this.health -= value;

        // The 1.0 forces floating point
        var healthFraction = (1.0 * this.health) / this.maxHealth;

        // If one parameter is provided, setTo() sets both
        // To prevent the asteroids from becoming way too small, an asteroid
        // is always dead at 20% size
        this.scale.setTo(0.2 + (this.origScaleFactor - 0.2) * healthFraction);
    };

    // Returns the energy that the asteroid has in total
    // TODO: use this for dropping energy orbs
    _Asteroid.prototype.calcEnergy = function() {
        var energy = (this.maxHealth - this.sunDamageTaken) / 4;

        if (this.type === _Asteroid.MORE_ENERGY) {
            energy *= 3;  // 3x boost
        }
        else if (this.type === _Asteroid.LESS_ENERGY) {
            energy /= 5;  // 20% only; pretty much not worth it :p
        }

        // Make sure it's at least 1 lol
        return Math.max(1, Math.floor(energy));
    };

    // Key and frame are unused for now
    function _Sun(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'sun');

        // Enable physics
        game.physics.arcade.enable(this);

        // Make the bounding box smaller and set its top-left to (50, 50);
        //this.body.setSize(500, 500, 50, 50);
    }

    _Sun.DIAMETER = 600;
    _Sun.MAX_HEALTH = 2000;

    // Delay after hit before it can start recovering
    _Sun.HEALTH_REGEN_DELAY = 5000;  // in ms
    // During regen, time interval for one unit of health recovery
    _Sun.HEALTH_REGEN_RATE = 100;  // in ms

    _Sun.prototype = Object.create(Phaser.Sprite.prototype);

    _Sun.prototype.health = _Sun.MAX_HEALTH;
    _Sun.prototype.regenDelay = 0;

    _Sun.prototype.subtractCooldowns = function(ms) {
        this.regenDelay -= ms;

        if (this.regenDelay <= 0) {
            this.health += 1;

            this.updateTint();

            if (this.health > _Sun.MAX_HEALTH) {
                this.health = _Sun.MAX_HEALTH;
            }

            this.regenDelay = _Sun.HEALTH_REGEN_RATE;
        }
    };

    _Sun.prototype.damage = function(value) {
        this.health -= value;

        // Set the delay until the Sun can start recovering
        this.regenDelay = _Sun.HEALTH_REGEN_DELAY;

        // Update the sun's appearance
        this.updateTint();
    };

    _Sun.prototype.updateTint = function() {
        // Make a bluish tint as the sun loses heat
        var healthFraction = 1.0 * this.health / _Sun.MAX_HEALTH;
        var red = Math.max(0, Math.floor(255 * healthFraction));
        var green = 63 + Math.max(0, Math.floor(192 * healthFraction));

        var color = Phaser.Color.getColor(red, green, 255);

        this.tint = color;
    }

    // Cores are energy pickups dropped by asteroids that are destroyed
    // by other means (except sun damage)
    function _Core(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'asteroid_core');

        // So that the core, when decaying, shrinks towards the center
        this.anchor.setTo(0.5, 0.5);
    }

    // Time before it starts decaying
    _Core.INITIAL_DECAY_WAIT = 5000;
    // Once it starts decaying, how often it loses 1 energy
    _Core.DECAY_INTERVAL = 200;

    _Core.fromAsteroid = function(ast) {
        var result = new _Core(ast.game, ast.x, ast.y);
        ast.game.physics.arcade.enable(result);

        result.body.center.x = ast.body.center.x;
        result.body.center.y = ast.body.center.y;

        var energy = Math.max(1, Math.floor(ast.calcEnergy() / 2));
        result.initialValue = energy;
        result.curValue = energy;
        // 45 is the highest possible energy from a core (120 / 4 * 3 / 2)
        result.origScaleFactor = 0.4 + (1.0 * energy / 45) * (ast.origScaleFactor - 0.4);
        result.scale.setTo(result.origScaleFactor);

        return result;
    };

    _Core.prototype = Object.create(Phaser.Sprite.prototype);

    _Core.prototype.initialValue = 10;
    _Core.prototype.curValue = 10;
    _Core.prototype.origScaleFactor = 1.0;
    _Core.prototype.decayCooldown = _Core.INITIAL_DECAY_WAIT;
    _Core.prototype.obtained = false;

    _Core.prototype.decay = function(ms) {
        this.decayCooldown -= ms;

        if (this.decayCooldown <= 0) {
            this.curValue -= 1;

            this.decayCooldown = _Core.DECAY_INTERVAL;

            // Update the scale
            var valueFraction = (1.0 * this.curValue) / this.initialValue;
            this.scale.setTo(0.4 + (this.origScaleFactor - 0.4) * valueFraction);
        }
    };

    var things = {Player: _Player, Asteroid: _Asteroid, Sun: _Sun,
                  Core: _Core};

    return things;
});
