// Holds the status bar code

define(['phaser', 'skills', 'things', 'utils'],
function(Phaser, Skills, Things, Utils) {
    'use strict';

    // state must be a GameplayState or else everything breaks
    function StatusBar(state) {
        this.state = state;
    }

    StatusBar.HEIGHT = 75;

    StatusBar.BOX_KEYS = ['skill_1', 'skill_2', 'skill_3', 'skill_4', 'skill_5'];
    StatusBar.BOX_START = 300;
    StatusBar.BOX_SIZE = 30;
    StatusBar.BOX_SPACING = 5;

    // Change this if new skills get added
    StatusBar.COOLDOWN_LENGTHS = [0,
                                  Skills.MissileSkill.MAX_COOLDOWN,
                                  Skills.HomingMissileSkill.MAX_COOLDOWN,
                                  Skills.TurretSkill.MAX_COOLDOWN,
                                  Skills.JamSkill.MAX_COOLDOWN];

    StatusBar.prototype = {
        constructor: StatusBar,

        state: null,

        barGroup: null,
        background: null,
        skillBoxes: null,
        skillCooldowns: null,
        selected: null,
        skill: null,
        cost: null,
        energy: null,
        fullness: null,
        sunHealth: null,
        time: null,
        level: null,
        pause: null,

        // Preload all the assets used by the status bar
        preload: function() {
            this.state.load.image('statusbar', 'assets/statusbar.png');

            this.state.load.image('skill_1', 'assets/skill_1.png');
            this.state.load.image('skill_2', 'assets/skill_2.png');
            this.state.load.image('skill_3', 'assets/skill_3.png');
            this.state.load.image('skill_4', 'assets/skill_4.png');
            this.state.load.image('skill_5', 'assets/skill_5.png');
            this.state.load.image('skill_selected', 'assets/skill_selected.png');
            this.state.load.image('cooldown_bar', 'assets/cooldown_bar.png');
        },

        // Create all the objects that the status bar is composed of and add
        // them to the state
        create: function() {
            // Create a group that will contain all the elements of the
            // status bar, and make sure they don't move when the camera does
            this.barGroup = this.state.add.group();
            this.barGroup.fixedToCamera = true;

            var yStart = this.state.camera.view.height - StatusBar.HEIGHT;
            this.background = this.state.add.image(0, yStart, 'statusbar');
            this.barGroup.add(this.background);

            // Add all the skill boxes and cooldown bars
            this.skillBoxes = [];
            this.skillCooldowns = [];

            for (var i = 0; i < this.state.player.allSkills.length; i++) {
                var boxPosX = this._getBoxPos(i);
                var boxPosY = yStart + 3;
                var cooldownY = boxPosY + StatusBar.BOX_SIZE + 3;

                var box = this.state.add.image(boxPosX, boxPosY, StatusBar.BOX_KEYS[i]);
                var cooldownBar = this.state.add.image(boxPosX, cooldownY, 'cooldown_bar');

                this.barGroup.add(box);
                this.barGroup.add(cooldownBar);

                this.skillBoxes.push(box);
                this.skillCooldowns.push(cooldownBar);
            }

            this.selected = this.state.add.image(this._getBoxPos(i) - 2,
                                                 yStart, 'skill_selected');

            this.skill = this.state.add.text(0, yStart, "",
                {fontSize: "16px", fill: "#ffff00"});

            this.cost = this.state.add.text(0, yStart + 20, "",
                {fontSize: "16px", fill: "#0088ff"});

            this.energy = this.state.add.text(0, yStart + 55, "",
                {fontSize: "16px", fill: "#0088ff"});

            this.fullness = this.state.add.text(150, yStart + 55, "",
                {fontSize: "16px", fill: "#66cc00"});

            this.sunHealth = this.state.add.text(300, yStart + 55, "",
                {fontSize: "16px", fill: "#ff8000"});

            this.time = this.state.add.text(600, yStart, "",
                {fontSize: "16px", fill: "#dddddd"});

            this.level = this.state.add.text(600, yStart + 20, "",
                {fontSize: "16px", fill: "#dddddd"});

            this.pause = this.state.add.text(600, yStart + 55, "P = Pause/Help",
                {fontSize: "16px", fill: "#ffffff"});

            this.barGroup.add(this.selected);
            this.barGroup.add(this.skill);
            this.barGroup.add(this.cost);
            this.barGroup.add(this.energy);
            this.barGroup.add(this.fullness);
            this.barGroup.add(this.sunHealth);
            this.barGroup.add(this.time);
            this.barGroup.add(this.level);
            this.barGroup.add(this.pause);
        },

        // Call this each frame to update it
        update: function() {
            var player = this.state.player;

            var skillName = player.curSkill.name;
            var skillCost = player.curSkill.cost;

            this.skill.text = "Skill: " + skillName;

            this.cost.text = "Cost: " + skillCost;
            // Change the text color to indicate that the player does not have
            // enough energy
            if (player.energy < skillCost) {
                this.cost.fill = "#FF0000";
            }
            else {
                this.cost.fill = "#0088FF";
            }

            this.energy.text = "Energy: " + Math.floor(player.energy) +
                               "/" + Math.floor(Things.Player.MAX_ENERGY);

            var fullnessPct = Phaser.Math.roundTo((1.0 * player.fullness /
                Things.Player.MAX_FULLNESS) * 100, -1).toFixed(1);
            this.fullness.text = "Fullness: " + fullnessPct + "%";
            this.fullness.fill = this._getFullnessColor(fullnessPct);

            var sunHealthPct = Math.max(0,
                Phaser.Math.roundTo((1.0 * this.state.sun.health /
                Things.Sun.MAX_HEALTH) * 100, -1)).toFixed(1);
            this.sunHealth.text = "Sun's Heat: " + sunHealthPct + "%";
            this.sunHealth.fill = this._getSunHealthColor(sunHealthPct);

            this.level.text = "Level: " + this.state.level;
            this.time.text = "Time: " + Utils.msToString(this.state.timeElapsed);

            // Update information about each skill
            for (var i = 0; i < player.allSkills.length; i++) {
                if (player.energy < player.allSkills[i].cost) {
                    this.skillBoxes[i].tint = 0x444444;
                }
                else {
                    this.skillBoxes[i].tint = 0xFFFFFF;
                }

                var cooldownBar = this.skillCooldowns[i];
                var maxCooldown = StatusBar.COOLDOWN_LENGTHS[i];
                var length;

                if (maxCooldown === 0) {  // Avoid divide by zero
                    length = 1.0;
                }
                else {
                    var curCooldown = player.allSkills[i].cooldown;
                    length = 1.0 / 1.0 - (1.0 * curCooldown / maxCooldown);
                }

                cooldownBar.scale.setTo(length, 1.0);
            }

            // Move the skill selected highlight
            var curSkill = player.curSkillIndex;
            this.selected.x = this._getBoxPos(curSkill) - 2;
        },

        _getBoxPos: function(i) {
            return StatusBar.BOX_START + i *
                   (StatusBar.BOX_SIZE + StatusBar.BOX_SPACING);
        },

        _getFullnessColor: function(pct) {
            return pct > 99 ? '#ff0000' :
                   pct > 90 ? '#ff6600' :
                   pct > 75 ? '#ffcc00' :
                   pct > 50 ? '#cccc00' : '#66cc00';
        },

        _getSunHealthColor: function(pct) {
            return pct > 50 ? '#ff8800' :
                   pct > 25 ? '#cccc00' :
                   pct > 10 ? '#99ccff' : '#3399ff';
        },

        destroy: function() {
            if (this.barGroup) {
                this.barGroup.destroy();
                this.barGroup = null;
            }

            if (this.skillBoxes) {
                this.skillBoxes = null;
            }

            if (this.skillCooldowns) {
                this.skillCooldowns = null;
            }

            this.skillCooldowns = null;
            this.selected = null;
            this.skill = null;
            this.cost = null;
            this.energy = null;
            this.fullness = null;
            this.sunHealth = null;
            this.time = null;
            this.level = null;
            this.pause = null;
        }
    };

    return StatusBar;
});
