// Taken from: https://github.com/photonstorm/phaser/blob/master/resources/Project%20Templates/RequireJS/src/game.js

define(['phaser', 'states'], function (Phaser, States) {
    'use strict';

    function Game() {

    }

    Game.prototype = {
        constructor: Game,

        game: null,

        start: function() {
            this.game = new Phaser.Game(800, 500, Phaser.AUTO, '', {
                preload: this.preload,
                create: this.create
            });
        },

        preload: function() {

        },

        create: function() {
            this.game.state.add('title', States.TitleState);
            this.game.state.add('story', States.StoryState);
            this.game.state.add('gameplay', States.GameplayState);
            this.game.state.add('gameover', States.GameOverState);
            this.game.state.start('title');
        }
    };

    return Game;
});
