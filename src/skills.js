// Skills and their associated objects are defined here

define(['phaser', 'utils'], function(Phaser, Utils) {
    'use strict';

    // Redefine some constants here
    // TODO: possibly move them all to a constants.js
    var NORMAL_ASTEROID = 0;
    var MORE_ENERGY_ASTEROID = 1;
    var LESS_ENERGY_ASTEROID = 2;

    var PLAYER_ENERGY_GAIN_RATE = 0.25;
    var PLAYER_FULLNESS_GAIN_RATE = 0.10;
    var PLAYER_MAX_FULLNESS = 200;

    function _EatSkill() { }

    _EatSkill.prototype.name = "Eat";  // Will be displayed on the status bar
    _EatSkill.prototype.cost = 0;  // Energy cost
    _EatSkill.prototype.cooldown = 0;  // Time in (ms) before next use
    _EatSkill.prototype.drawRange = false;  // If effect area should be drawn

    _EatSkill.prototype.lastEatTime = 0;  // Time when player last ate

    // State should be a GameplayState; this applies for all skills
    _EatSkill.prototype.use = function(state) {
        var count = state.targetAsteroids.length;
        if (count === 0) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        // If the player is full, don't do anything
        if (Math.floor(state.player.fullness) >= PLAYER_MAX_FULLNESS) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        for (var i = 0; i < count; i++) {
            var ast = state.targetAsteroids[i];
            // Damage the asteroid
            ast.damage(1);

            // Gain energy depending on the asteroid
            if (ast.type === MORE_ENERGY_ASTEROID) {
                state.player.gainEnergy(PLAYER_ENERGY_GAIN_RATE * 3);
            }
            else if (ast.type === LESS_ENERGY_ASTEROID) {
                state.player.gainEnergy(PLAYER_ENERGY_GAIN_RATE / 5);
            }
            else {
                state.player.gainEnergy(PLAYER_ENERGY_GAIN_RATE);
            }

            // Gain fullness too
            state.player.gainFullness(PLAYER_FULLNESS_GAIN_RATE);

            // Play the sound
            Utils.playSoundIfIdle(state.playerEatSound);
            this.lastEatTime = state.timeElapsed;

            if (ast.health === 0) {
                // Remove the asteroid from the group and destroy (by passing
                // 'true' as the second parameter) the sprite itself.
                state.asteroids.remove(ast, true);
            }
        }
    };

    _EatSkill.prototype.getAreaRect = function() {
        return new Phaser.Rectangle(0, 0, 64, 64);
    };

    // Key and frame are not used yet
    function _Missile(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'missile');

        // Enable physics for the missile
        game.physics.arcade.enable(this);
    }

    _Missile.SPEED = 400;

    // Given an asteroid, returns the 'equivalent' missile that would be
    // generated from that asteroid
    _Missile.fromAsteroid = function(ast) {
        // Use the asteroid's stats as the basis for the missile's stats
        // Thus, larger asteroids make better weapons
        var result = new _Missile(ast.game, ast.body.x, ast.body.y);
        //ast.game.physics.arcade.enable(result);  // Otherwise there will be no body

        result.body.center.x = ast.body.center.x;
        result.body.center.y = ast.body.center.y;

        result.body.velocity.x = -ast.body.velocity.x; // Asteroids go left, so go right
        result.body.velocity.y = -ast.body.velocity.y;
        result.body.velocity.setMagnitude(_Missile.SPEED);

        result.scale.setTo(ast.scale.x);  // x and y are the same
        result.health = Math.max(25, ast.health);

        return result;
    };

    _Missile.prototype = Object.create(Phaser.Sprite.prototype);

    _Missile.prototype.health = 0;  // Determines the missile's power
    // Prevents hitting two things with one missile; don't use outside of the
    // class, but use checkIfAlreadyHit() instead b/c the underlying
    // implementation might change
    _Missile.prototype._alreadyHit = false;

    // Called when the missile has hit something
    _Missile.prototype.hitAsteroid = function(ast) {
        ast.damage(this.health);

        // Play the hit sound; terrible hack
        ast.game.state.getCurrentState().missileHitSound.play();

        // Will kill the missile
        this.health = 0;
        // Don't let one missile hit two things
        this._alreadyHit = true;
    };

    // Call if you want to check if a projectile has already hit something;
    // helps prevent one missile hitting two things at once
    _Missile.prototype.checkIfAlreadyHit = function() {
        return this.alreadyHit;
    };

    _Missile.prototype.think = function(state) {
        // Not used; missiles have no other special behavior
    };

    function _MissileSkill() { }

    _MissileSkill.MAX_COOLDOWN = 1000;

    _MissileSkill.prototype.name = "Missiles";
    _MissileSkill.prototype.cost = 30;
    _MissileSkill.prototype.cooldown = 0;  // In milliseconds
    _MissileSkill.prototype.drawRange = true;

    _MissileSkill.prototype.use = function(state) {
        var count = state.targetAsteroids.length;
        if (count === 0) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        // Turn asteroids into missiles
        for (var i = 0; i < count; i++) {
            var ast = state.targetAsteroids[i];
            var missile = _Missile.fromAsteroid(ast);

            // Add the missile to the group
            state.projectiles.add(missile);

            // Remove and destroy the asteroid
            state.asteroids.remove(ast, true);
        }

        // Play the sound
        state.missileShootSound.play();

        // Prevent usage of the skill again for a few seconds;
        this.cooldown = _MissileSkill.MAX_COOLDOWN;
    };

    _MissileSkill.prototype.getAreaRect = function() {
        return new Phaser.Rectangle(0, 0, 320, 320);
    };

    // Key and frame are not used yet
    function _HomingMissile(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'homing_missile');

        // Enable physics for the missile
        game.physics.arcade.enable(this);
    }

    // Given an asteroid, returns the 'equivalent' homing missile that would be
    // generated from that asteroid
    _HomingMissile.fromAsteroid = function(ast) {
        // Use the asteroid's stats as the basis for the missile's stats
        // Thus, larger asteroids make better weapons
        var result = new _HomingMissile(ast.game, ast.body.x, ast.body.y);

        result.body.center.x = ast.body.center.x;
        result.body.center.y = ast.body.center.y;

        result.body.velocity.x = -ast.body.velocity.x; // Asteroids go left, so go right
        result.body.velocity.y = -ast.body.velocity.y;
        result.body.velocity.setMagnitude(_HomingMissile.SPEED);

        result.scale.setTo(ast.scale.x);  // x and y are the same
        result.health = Math.max(25, ast.health);

        return result;
    };

    // Creates a homing missile from a turret launch
    _HomingMissile.fromTurret = function(turret) {
        var result = new _HomingMissile(turret.game, turret.body.x, turret.body.y);

        result.body.center.x = turret.body.center.x;
        result.body.center.y = turret.body.center.y;

        result.body.velocity.x = _HomingMissile.SPEED;
        result.body.velocity.y = 0;

        result.scale.setTo(turret.scale.x);
        result.health = Math.max(25, turret.astHealth);

        return result;
    }

    _HomingMissile.prototype = Object.create(Phaser.Sprite.prototype);

    _HomingMissile.TURN_RADIANS = Math.PI / 16;  // Per frame
    _HomingMissile.SPEED = 500;
    _HomingMissile.SEEK_RADIUS = 800;

    _HomingMissile.prototype.health = 0;  // Determines the missile's power
    // The homing missile's target
    _HomingMissile.prototype.target = null;
    // Prevents hitting two things with one missile; don't use outside of the
    // class, but use checkIfAlreadyHit() instead b/c the underlying
    // implementation might change
    _HomingMissile.prototype._alreadyHit = false;

    // Called when the missile has hit something
    _HomingMissile.prototype.hitAsteroid = function(ast) {
        ast.damage(this.health);

        // Play the hit sound; terrible hack
        ast.game.state.getCurrentState().missileHitSound.play();

        // Will kill the missile
        this.health = 0;
        // Don't let one missile hit two things
        this._alreadyHit = true;
    };

    // Call if you want to check if a projectile has already hit something;
    // helps prevent one missile hitting two things at once
    _HomingMissile.prototype.checkIfAlreadyHit = function() {
        return this.alreadyHit;
    };

    _HomingMissile.prototype.think = function(state) {
        // Copy the asteroids group
        var asteroids = state.asteroids.children.slice(0);

        // If there are no asteroids right now, don't think anymore
        if (asteroids.length === 0) {
            return;
        }

        // Find the closest untargeted asteroid if it does not have a target
        if (!this.target || this.target.health <= 0) {
            var closestDistance = _HomingMissile.SEEK_RADIUS;

            for (var i = 0; i < asteroids.length; i++) {
                var distance = Math.abs(this.body.center.distance(asteroids[i].body.center));

                if (distance < closestDistance && !asteroids[i].targeted) {
                    closestDistance = distance;
                    this.target = asteroids[i];
                }
            }
        }

        // If we still don't have a target, don't do anything
        if (!this.target) {
            return;
        }

        // Mark this asteroid as being targeted
        this.target.targeted = true;

        // Watch for anything that can delete the asteroid, and notify the
        // missile if it does disappear
        this.target.events.onDestroy.add(function() {
            this.target = null;
        }, this);

        // Do some stupid math (all in radians)
        // See: http://gamemechanicexplorer.com/#homingmissiles-1
        // I'm not that smart to come up with this myself :p

        // The angle we want
        var desiredAngle = this.body.center.angle(this.target.body.center);
        // The difference between the angle we want and our current one
        var diffAngle = desiredAngle - this.rotation;

        // Make sure the difference is within -pi/2 to pi/2
        if (diffAngle > Math.PI) {
            diffAngle -= Math.PI * 2;
        }
        if (diffAngle < -Math.PI) {
            diffAngle += Math.PI * 2;
        }

        // Turn depending on the direction we differ by
        if (diffAngle > 0) {
            this.rotation += _HomingMissile.TURN_RADIANS;
        }
        else {
            this.rotation -= _HomingMissile.TURN_RADIANS;
        }

        // Don't turn if we're good enough
        if (Math.abs(diffAngle) < _HomingMissile.TURN_RADIANS) {
            this.rotation = desiredAngle;
        }

        var mag = this.body.velocity.getMagnitude();

        state.physics.arcade.velocityFromAngle(
            Phaser.Math.radToDeg(this.rotation), mag, this.body.velocity);
    };

    function _HomingMissileSkill() { }

    _HomingMissileSkill.MAX_COOLDOWN = 5000;

    _HomingMissileSkill.prototype.name = "Homing Missiles";
    _HomingMissileSkill.prototype.cost = 60;
    _HomingMissileSkill.prototype.cooldown = 0;  // In milliseconds
    _HomingMissileSkill.prototype.drawRange = true;

    _HomingMissileSkill.prototype.use = function(state) {
        var count = state.targetAsteroids.length;
        if (count === 0) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        // Turn asteroids into homing missiles
        for (var i = 0; i < count; i++) {
            var ast = state.targetAsteroids[i];
            var missile = _HomingMissile.fromAsteroid(ast);

            // Add the homing missile to the group
            // The homing missile will find a target later in the loop
            state.projectiles.add(missile);

            // Play the sound
            state.missileShootSound.play();

            // Remove and destroy the asteroid
            state.asteroids.remove(ast, true);
        }

        // Prevent usage of the skill again for a few seconds;
        this.cooldown = _HomingMissileSkill.MAX_COOLDOWN;
    };

    _HomingMissileSkill.prototype.getAreaRect = function() {
        return new Phaser.Rectangle(0, 0, 256, 256);
    };

    // Key and frame are not used yet
    function _Turret(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'turret');

        // Enable physics for the turret
        game.physics.arcade.enable(this);
    }

    // Given an asteroid, returns the 'equivalent' turret that would be
    // generated from that asteroid
    _Turret.fromAsteroid = function(ast) {
        // Use the asteroid's stats as the basis for the turret's stats
        // Thus, larger asteroids make better weapons
        var result = new _Turret(ast.game, ast.body.x, ast.body.y);

        result.body.center.x = ast.body.center.x;
        result.body.center.y = ast.body.center.y;

        result.body.velocity.x = 60;
        result.body.velocity.y = 0;

        result.scale.setTo(ast.scale.x);  // x and y are the same
        var tempHealth = Math.floor(Math.min(5, ast.health / 20));
        if (tempHealth === 0) {
            result.health = 1;
        }
        else {
            result.health = tempHealth;
        }

        result.astHealth = ast.health;

        return result;
    };

    _Turret.prototype = Object.create(Phaser.Sprite.prototype);

    _Turret.prototype.health = 0;  // Determines the amount of fires left for this turret.
    _Turret.prototype.astHealth = 0; // Health of the asteroid this turret was spawned from.
    _Turret.prototype.shootTimer = 0; // Timer increments every frame. Creates delay between firing.

    // Prevents hitting two things with one turret; don't use outside of the
    // class, but use checkIfAlreadyHit() instead b/c the underlying
    // implementation might change
    _Turret.prototype._alreadyHit = false;

    // Called when the turret has hit something
    _Turret.prototype.hitAsteroid = function(ast) {
        ast.damage(this.health);

        // Use the missile hit sound as the turret death sound
        ast.game.state.getCurrentState().missileHitSound.play();

        // Will kill the turret
        this.health = 0;
        // Don't let one turret hit two things
        this._alreadyHit = true;
    };

    // Call if you want to check if a projectile has already hit something;
    // helps prevent one turret hitting two things at once
    _Turret.prototype.checkIfAlreadyHit = function() {
        return this.alreadyHit;
    };

    _Turret.prototype.think = function(state) {
        this.shootTimer++;

        if (this.shootTimer >= 1 * 100) {
            var missile = _HomingMissile.fromTurret(this);
            state.projectiles.add(missile);
            this.health--;  // Subtract health at the end of firing a missile.
            this.shootTimer = 0;  // Reset the Timer.

            // Play the sound
            state.missileShootSound.play();
        }
    };

    function _TurretSkill() { }

    _TurretSkill.MAX_COOLDOWN = 15000;

    _TurretSkill.prototype.name = "Turret";
    _TurretSkill.prototype.cost = 120;
    _TurretSkill.prototype.cooldown = 0;  // In milliseconds
    _TurretSkill.prototype.drawRange = true;

    _TurretSkill.prototype.use = function(state) {
        var count = state.targetAsteroids.length;
        if (count === 0) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        // Turn asteroids into turrets
        for (var i = 0; i < count; i++) {
            var ast = state.targetAsteroids[i];
            var turret = _Turret.fromAsteroid(ast);

            // Adds the turret to projectiles.
            // This is a pretty hacky way to do it,
            // but we need the turret to work with collisions and detection
            // without adding a ton of code right now.
            state.projectiles.add(turret);

            // Play the sound
            state.turretCreateSound.play();

            // Remove and destroy the asteroid
            state.asteroids.remove(ast, true);
        }

        // Prevent usage of the skill again for a few seconds;
        this.cooldown = _TurretSkill.MAX_COOLDOWN;
    };

    _TurretSkill.prototype.getAreaRect = function() {
        return new Phaser.Rectangle(0, 0, 256, 256);
    };

    // Key and frame are not used yet
    function _Jam(game, x, y, key, frame) {
        Phaser.Sprite.call(this, game, x, y, 'jam');

        // Enable physics for the turret
        game.physics.arcade.enable(this);
    }

    // Given am asteroid.
    _Jam.fromAsteroid = function(ast) {
        // Use the asteroid's stats as the basis for the turret's stats
        // Thus, larger asteroids make better weapons
        var result = new _Jam(ast.game, ast.body.x, ast.body.y);

        result.body.center.x = ast.body.center.x;
        result.body.center.y = ast.body.center.y;

        result.body.velocity.x = -ast.body.velocity.x;
        result.body.velocity.y = -ast.body.velocity.y;

        result.scale.setTo(ast.scale.x);  // x and y are the same

        return result;
    };

    _Jam.prototype = Object.create(Phaser.Sprite.prototype);

    _Jam.prototype.health = 100;

    // Called when the jam has hit something
    _Jam.prototype.hitAsteroid = function(ast) {
        ast.damage(8);

        Utils.playSoundIfIdle(ast.game.state.getCurrentState().jamAttackSound);
    };

    // Call if you want to check if a projectile has already hit something
    _Jam.prototype.checkIfAlreadyHit = function() {
        return false;
    };

    _Jam.prototype.think = function(state) {
        // Jam doesn't need no thinking; it just is
    };

    function _JamSkill() { }

    _JamSkill.MAX_COOLDOWN = 20000;

    _JamSkill.prototype.name = "Jam";
    _JamSkill.prototype.cost = 200;
    _JamSkill.prototype.cooldown = 0;  // In milliseconds
    _JamSkill.prototype.drawRange = true;

    _JamSkill.prototype.use = function(state) {
        var count = state.targetAsteroids.length;
        if (count === 0) {
            Utils.playSoundIfIdle(state.noUseSound);
            return;
        }

        for (var i = 0; i < count; i++) {
            var ast = state.targetAsteroids[i];
            var jam = _Jam.fromAsteroid(ast);
            state.projectiles.add(jam);

            // Remove and destroy the asteroid
            state.asteroids.remove(ast, true);

            // Play the sound
            state.jamCreateSound.play();
        }

        // Prevent usage of the skill again for a few seconds
        this.cooldown = _JamSkill.MAX_COOLDOWN;
    };

    _JamSkill.prototype.getAreaRect = function() {
        return new Phaser.Rectangle(0, 0, 600, 600);
    };

    var skillObjs = {EatSkill: _EatSkill,
                     Missile: _Missile,
                     MissileSkill: _MissileSkill,
                     HomingMissile: _HomingMissile,
                     HomingMissileSkill: _HomingMissileSkill,
                     Turret: _Turret,
                     TurretSkill: _TurretSkill,
                     Jam: _Jam,
                     JamSkill: _JamSkill,};

    return skillObjs;
});
